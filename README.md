# Приложение микросервиса между пользователем и почтой

## Описание проекта

Этот проект представляет собой микросервис, который обеспечивает взаимодействие между пользователем и почтовым сервисом. Микросервис использует Swagger для документирования API и Java 17 в качестве основного языка программирования. Кроме того, RabbitMQ используется для асинхронного обмена сообщениями между микросервисами.

## Установка и запуск

1. Склонируйте репозиторий на свой локальный компьютер.
2. Убедитесь, что на вашем компьютере установлена Java 17 и RabbitMQ.
3. В файле `application.properties` укажите необходимые настройки, такие как URL к базе данных, настройки RabbitMQ и другие.
4. Запустите проект 
5. Приложение будет запущено на порту `8082` и `8083`.

## Использование

Документация API доступна по адресу `http://localhost:8082/swagger-ui.html` , `http://localhost:8083/swagger-ui.html`. В ней вы сможете ознакомиться со всеми доступными эндпоинтами и их описанием.

## Взаимодействие с RabbitMQ

Микросервис использует RabbitMQ для асинхронного обмена сообщениями с другими микросервисами. Для отправки сообщений используется exchange `email-exchange`.

Пример отправки сообщения:

```java
@Service
public class EmailService {

    final EmailRepository emailRepository;
    final JavaMailSender emailSender;

    public EmailService(EmailRepository emailRepository, JavaMailSender emailSender) {
        this.emailRepository = emailRepository;
        this.emailSender = emailSender;
    }
    @Value(value = "${spring.mail.username}")
    private String emailFrom;

    @Transactional
    public EmailModel sendEmail(EmailModel emailModel){
        try {
            emailModel.setSendDateEmail(LocalDateTime.now());
            emailModel.setEmailFrom(emailFrom);

            SimpleMailMessage message = new SimpleMailMessage();
            message.setTo(emailModel.getEmailTo());
            message.setSubject(emailModel.getSubject());
            message.setText(emailModel.getText());
            emailSender.send(message);

            emailModel.setStatusEmail(StatusEmail.SENT);
        }catch (MailException e){
            emailModel.setStatusEmail(StatusEmail.ERROR);
        }finally {
            return emailRepository.save(emailModel);
        }
    }

}
```

Чтобы обработать сообщение, необходимо создать слушателя с помощью аннотации `@RabbitListener`:

```java
@Component
public class EmailConsumer {
    final EmailService emailService;

    public EmailConsumer(EmailService emailService) {
        this.emailService = emailService;
    }

    @RabbitListener(queues = "${broker.queue.email.name}")
    public void listenEmailQueue(@Payload EmailRecordDto emailRecordDto){
        var emailModel = new EmailModel();
        BeanUtils.copyProperties(emailRecordDto,emailModel);
        emailService.sendEmail(emailModel);
    }
}